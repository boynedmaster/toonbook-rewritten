"""toonbookrewritten URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from toonbook.views import *

urlpatterns = [
	url(r'^$', IndexView.as_view),
	url(r'^login/$', LoginView.as_view),
	url(r'^register/$', RegisterView.as_view),
	url(r'^logout/$', LogoutView.as_view),
	url(r'^verify/(?P<key>.+)/$', ActivationView.as_view),
	url(r'^profile/(?P<profile>.+)/panel/$', ProfilePanelView.as_view),
	url(r'^profile/(?P<profile>.+)/images/$', ProfileImageView.as_view),
	url(r'^profile/(?P<profile>.+)/images/add/$', ProfileImageAddView.as_view),
	url(r'^profile/(?P<profile>.+)/images/(?P<album>.+)/$', ProfileAlbumView.as_view),
	url(r'^profile/(?P<profile>.+)/$', ProfileView.as_view),
	url(r'^post/(?P<post>\d+)/$', PostView.as_view),
	url(r'^notifications/$', NotificationsView.as_view),
	url(r'^notifications/read_all/$', NotificationsReadAllView.as_view),
	url(r'^write_post/$', WritePostView.as_view),
	url(r'^get_posts/$', GetPostView.as_view),
	url(r'^comment/(?P<comment>\d+)/$', CommentView.as_view),
	url(r'^write_comment/$', WriteCommentView.as_view),
	url(r'^get_comments/$', GetCommentView.as_view),
	url(r'^admin_panel/$', AdminView.as_view),
	url(r'^admin_panel/filter/$', AdminFilterView.as_view),
	url(r'^admin_panel/approve/$', AdminApproveView.as_view),
	url(r'^admin/', include(admin.site.urls)),
]
