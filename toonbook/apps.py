from django.apps import AppConfig


class ToonbookConfig(AppConfig):
    name = 'toonbook'
