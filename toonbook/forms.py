import base64
import bcrypt
from datetime import date
from django import forms
from django.core.exceptions import ValidationError
from django.forms import extras
from django.utils.safestring import mark_safe
from nocaptcha_recaptcha.fields import NoReCaptchaField
from toonbook.models import *

class PlainTextWidget(forms.Widget):
	def render(self, _name, value, attrs=None):
		return ("<h3>%s</h3>" % mark_safe(value) if value is not None else "") + "<hr>"

class ToonForm(forms.ModelForm):
	captcha = NoReCaptchaField()
	password = forms.CharField(widget=forms.PasswordInput())
	retype_password = forms.CharField(widget=forms.PasswordInput(), label="Retype password")
	birthday = forms.DateField(widget=extras.SelectDateWidget(years=([i for i in reversed(range(date.today().year - 100, date.today().year - 12))])))
	
	def clean(self):
		try:
			password = self.cleaned_data["password"]
			retype_password = self.cleaned_data["retype_password"]
			
			if password != retype_password:
				raise ValidationError("The passwords do not match")
			
			username = self.cleaned_data["username"]
			self.cleaned_data["password"] = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
			
			if not username.isalnum():
				raise ValidationError("Usernames must be alphanumeric")
			
			if Toon.objects.filter(username=username):
				raise ValidationError("The username %s is already taken" % username)
		except KeyError:
			raise ValidationError("The password or retype password field was blank")
		
		return self.cleaned_data
	
	class Meta:
		model = Toon
		fields = ["username", "password", "retype_password", "email", "birthday", "toon_name", "captcha"]

class ToonEditForm(forms.ModelForm):
	about_me = forms.CharField(required=False, widget=forms.Textarea(attrs={"rows": 4, "maxlength": 1000, "style": "display: block"}))
	laff = forms.ChoiceField(choices=((x, x) for x in range(15, 138)))
	team = forms.ChoiceField(choices=((x, Team.labels.value[x]) for x in range(0, len(Team) - 1)))
	species = forms.ChoiceField(choices=((x, Species.labels.value[x]) for x in range(0, len(Species) - 1)))
	toon_up = forms.ChoiceField(choices=((x, Toonup.labels.value[x]) for x in range(0, len(Toonup) - 1)))
	trap = forms.ChoiceField(choices=((x, Trap.labels.value[x]) for x in range(0, len(Trap) - 1)))
	lure = forms.ChoiceField(choices=((x, Lure.labels.value[x]) for x in range(0, len(Lure) - 1)))
	sound = forms.ChoiceField(choices=((x, Sound.labels.value[x]) for x in range(0, len(Sound) - 1)))
	throw = forms.ChoiceField(choices=((x, Throw.labels.value[x]) for x in range(0, len(Throw) - 1)))
	squirt = forms.ChoiceField(choices=((x, Squirt.labels.value[x]) for x in range(0, len(Squirt) - 1)))
	drop = forms.ChoiceField(choices=((x, Drop.labels.value[x]) for x in range(0, len(Drop) - 1)))
	sellbot = forms.ChoiceField(choices=((x, SellbotSuit.labels.value[x]) for x in range(0, len(SellbotSuit) - 1)))
	cashbot = forms.ChoiceField(choices=((x, CashbotSuit.labels.value[x]) for x in range(0, len(CashbotSuit) - 1)))
	lawbot = forms.ChoiceField(choices=((x, LawbotSuit.labels.value[x]) for x in range(0, len(LawbotSuit) - 1)))
	bossbot = forms.ChoiceField(choices=((x, BossbotSuit.labels.value[x]) for x in range(0, len(BossbotSuit) - 1)))
	separator_notify = forms.CharField(widget=PlainTextWidget(), initial="Notify me when someone...", label="", required=False)
	notify_likes_post = forms.BooleanField(label="Likes my post", required=False)
	notify_likes_comment = forms.BooleanField(label="Likes my comment", required=False)
	notify_comments = forms.BooleanField(label="Commented on my post", required=False)
	notify_liked_commented = forms.BooleanField(label="Likes a post I commented on", required=False)
	notify_comment_commented = forms.BooleanField(label="Commented on a post I commented on", required=False)
	separator_picture = forms.CharField(widget=PlainTextWidget(), label="", required=False)
	picture = forms.ImageField(required=False)
	separator_end = forms.CharField(widget=PlainTextWidget(), label="", required=False)
	
	def clean_picture(self):
		picture = self.cleaned_data["picture"]
		
		if picture != None:
			if picture.size > 2 * 1024 * 1024:
				raise ValidationError("Profile picture is too big. Keep the file size under 2MB")
			
			file = open(picture.temporary_file_path(), 'rb').read()
			img = base64.b64encode(file)
			
			return "data:image/png;base64,%s" % img
		else:
			return ""
	
	class Meta:
		model = Toon
		fields = ["about_me", "laff", "team", "species", "toon_up", "trap", "lure", "sound", "throw", "squirt", "drop", "sellbot", "cashbot", "lawbot", "bossbot", "separator_notify", "notify_likes_post", "notify_likes_comment", "notify_comments", "notify_liked_commented", "notify_comment_commented", "separator_picture", "picture", "separator_end"]

class LoginForm(forms.Form):
	user = None
	username = forms.CharField(max_length=32)
	password = forms.CharField(widget=forms.PasswordInput())
	
	def clean(self):
		try:
			user = Toon.objects.get(username=self.cleaned_data["username"])
		except:
			raise ValidationError("Could not find a user with that username or password.")
		
		try:
			if bcrypt.hashpw(self.cleaned_data["password"].encode("utf-8"), user.password.encode("utf-8")) != user.password.encode("utf-8"):
				raise ValidationError("Could not find a user with that username or password.")
		except KeyError:
			raise ValidationError("The password or retype password field was blank")
		
		if user.activation != "":
			raise ValidationError("Your email has not been activated yet.")
		
		if not user.approved:
			raise ValidationError("Your account has not been approved by an admin yet. Please try again in a few days.")
		
		self.user = user

class PostForm(forms.ModelForm):
	post = forms.CharField(widget=forms.Textarea(attrs={"rows": 4, "maxlength": 300}))
	image_attachment = forms.ImageField(required=False)

	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop("user")
		super(PostForm, self).__init__(*args, **kwargs)
	
	def clean_image_attachment(self):
		picture = self.cleaned_data["image_attachment"]

		if picture != None:
			try:
				if picture.size > 2 * 1024 * 1024:
					raise ValidationError("Image attachment is too large. Please keep it under 2MB.")
				
				file = open(picture.temporary_file_path(), 'rb').read()
				img = "data:image/png;base64,%s" % base64.b64encode(file)
				
				image = Image.objects.create(image=img, album=Album.objects.filter(userid=self.user.pk).first().pk, userid=self.user.pk)
				return image
			except:
				return None
		else:
			return None

	class Meta:
		model = Post
		fields = ["post", "broadcast", "image_attachment"]

class CommentForm(forms.ModelForm):
	post = forms.IntegerField(widget=forms.HiddenInput())
	comment = forms.CharField(widget=forms.Textarea(attrs={"rows": 4, "maxlength": 300}))
	
	def clean_post(self):
		value = self.cleaned_data["post"]
		
		if not Post.objects.filter(pk=value):
			raise ValidationError("Post does not exist.")
		
		return value
	
	class Meta:
		model = Comment
		fields = ["comment", "post"]

class AlbumForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop("user")
		super(AlbumForm, self).__init__(*args, **kwargs)
	
	def clean_title(self):
		if Album.objects.filter(userid=self.user.pk).count() > 10:
			raise ValidationError("You can only have 10 albums.")

		return self.cleaned_data["title"]

	class Meta:
		model = Album
		fields = ["title"]

class FilterForm(forms.ModelForm):
	class Meta:
		model = Filter
		exclude = []
