from django import template
from ..models import *
import lxml.html
import re

register = template.Library()

#this is such a terrible way of doing it lol
@register.simple_tag
def clean_comments(value, standalone, post_id):
	if standalone:
		return value
	
	try:
		html = lxml.html.document_fromstring(value)
		comments = html.cssselect(".comment-tr")
		comments_cut = comments[-3:]
	except lxml.etree.XMLSyntaxError:
		return value
	
	ret = "<tr><td><a href='/post/%d/'>%d comment%s hidden</a></td></tr>" % (post_id, len(comments) - 3, "s" if len(comments) > 4 else "") if len(comments) > 3 else ""
	
	for comment in comments_cut:
		ret += lxml.etree.tostring(comment)
	
	return ret

@register.simple_tag
def toon_enum(enum_type, value):
	check = None
	
	if enum_type == "species":
		check = Species
	elif enum_type == "team":
		check = Team
	elif enum_type == "toonup":
		check = Toonup
	elif enum_type == "trap":
		check = Trap
	elif enum_type == "lure":
		check = Lure
	elif enum_type == "sound":
		check = Sound
	elif enum_type == "throw":
		check = Throw
	elif enum_type == "squirt":
		check = Squirt
	elif enum_type == "drop":
		check = Drop
	elif enum_type == "sellbot":
		check = SellbotSuit
	elif enum_type == "cashbot":
		check = CashbotSuit
	elif enum_type == "lawbot":
		check = LawbotSuit
	elif enum_type == "bossbot":
		check = BossbotSuit
		
	if check == None:
		return value
	else:
		return check.labels.value[value]

@register.filter
def post_filter(value):
	filtered = value
	
	for filter in Filter.objects.all():
		filtered = re.compile(re.escape(filter.replace), re.IGNORECASE).sub(filter.with_this, filtered)
	
	return filtered
