# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0014_filter'),
    ]

    operations = [
        migrations.RenameField(
            model_name='filter',
            old_name='_with',
            new_name='with_this',
        ),
    ]
