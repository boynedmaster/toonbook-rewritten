# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0022_post_image_attachment'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ImageAttachment',
            new_name='Image',
        ),
    ]
