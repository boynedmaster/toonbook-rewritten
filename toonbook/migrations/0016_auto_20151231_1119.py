# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0015_auto_20151230_1754'),
    ]

    operations = [
        migrations.AlterField(
            model_name='toon',
            name='about_me',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
