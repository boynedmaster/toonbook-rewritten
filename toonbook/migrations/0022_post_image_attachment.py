# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0021_album_imageattachment'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='image_attachment',
            field=models.ForeignKey(to='toonbook.ImageAttachment', null=True),
        ),
    ]
