# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0023_auto_20160102_1800'),
    ]

    operations = [
        migrations.AddField(
            model_name='toon',
            name='notify_comment_commented',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='toon',
            name='notify_comments',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='toon',
            name='notify_liked_commented',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='toon',
            name='notify_likes_comment',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='toon',
            name='notify_likes_post',
            field=models.BooleanField(default=True),
        ),
    ]
