# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0024_auto_20160107_1805'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creator', models.IntegerField()),
                ('userid', models.IntegerField()),
                ('link', models.CharField(max_length=255)),
                ('notif_type', models.IntegerField()),
                ('show', models.BooleanField(default=True)),
            ],
        ),
    ]
