# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toonbook', '0016_auto_20151231_1119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='toon',
            name='activation',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
