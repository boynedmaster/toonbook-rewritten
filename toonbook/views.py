from django.conf import settings
from django.core.mail import send_mail
from django.db.models.base import ModelState
from django.forms.models import model_to_dict
from django.http import HttpResponse, JsonResponse, Http404, HttpResponseForbidden, QueryDict
from django.shortcuts import render, redirect
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.utils.decorators import classonlymethod
from toonbook.forms import *
from toonbook.models import *
from PIL import Image
import base64
import datetime
import decimal
import json
import os
import random
import string
import tempfile

#Checks if a user is logged in by their request
is_logged_in = lambda request: request.session.get("logged_in") != None

#Gets the logged in user by their request
get_user = lambda request: Toon.objects.get(pk=request.session.get("userid"))

#OOP View that should be used for all views
#When needing to use get, post, put, patch, and delete methods, you must put a classonlymethod decorator
class RESTView:
	url_parameters = {}
	
	@classonlymethod
	def as_view(self, request, **kwargs):
		for key in kwargs:
			self.url_parameters[key] = kwargs[key]
		
		if request.method == "GET":
			return self.get(request)
		elif request.method == "POST":
			return self.post(request)
		elif request.method == "PUT":
			return self.put(request)
		elif request.method == "PATCH":
			return self.patch(request)
		elif request.method == "DELETE":
			return self.delete(request)
		else:
			return HttpResponse(status=501)

	@classonlymethod
	def get(self, request):
		return HttpResponse(status=501)

	@classonlymethod
	def post(self, request):
		return HttpResponse(status=501)

	@classonlymethod
	def put(self, request):
		return HttpResponse(status=501)
	
	@classonlymethod
	def patch(self, request):
		return HttpResponse(status=501)

	@classonlymethod
	def delete(self, request):
		return HttpResponse(status=501)

#The same as render, but renders with all of the default variables
def render_tb(request, tpl, variables):
	logged_in = is_logged_in(request)
	
	default_variables = {
		"logged_in": logged_in
	}
	
	if logged_in:
		user = get_user(request)

		default_variables["user"] = user
		default_variables["notifications"] = Notification.objects.filter(userid=user.pk, show=True)
	
	new_variables = variables.copy()
	new_variables.update(default_variables)
	
	return render(request, tpl, new_variables)

#JSON Encoders
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
       if hasattr(obj, 'isoformat'):
           return obj.isoformat()
       elif isinstance(obj, decimal.Decimal):
           return float(obj)
       elif isinstance(obj, ModelState):
           return None
       else:
           return json.JSONEncoder.default(self, obj)

class IndexView(RESTView):
	@classonlymethod
	def get(self, request):
		vars = {
			"title": "Home"
		}
		
		if is_logged_in(request):
			vars["write_post"] = PostForm(user=get_user(request))
			vars["posts"] = GetPostView().get_posts(request, get_user(request), "html", 10)
		
		return render_tb(request, "index.html", vars)

class LoginView(RESTView):
	@classonlymethod
	def get(self, request):
		if not is_logged_in(request):
			return render_tb(request, "login.html", {
				"title": "Login",
				"form": LoginForm()
			})
		else:
			return redirect("/")
	
	@classonlymethod
	def post(self, request):
		if not is_logged_in(request):
			form = LoginForm(request.POST)
			
			if form.is_valid():
				request.session["logged_in"] = True
				request.session["userid"] = form.user.pk
				return redirect("/")
			else:
				return render_tb(request, "login.html", {
					"title": "Login",
					"form": form
				})
		else:
			return redirect("/")

class RegisterView(RESTView):
	@classonlymethod
	def get(self, request):
		if not is_logged_in(request):
			return render_tb(request, "register.html", {
				"title": "Register",
				"form": ToonForm
			})
		else:
			return redirect("/")
	
	@classonlymethod
	def post(self, request):
		if not is_logged_in(request):
			form = ToonForm(request.POST)
			
			if form.is_valid():
				activation_url = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(100))
				context = Context({
						"username": request.POST["username"],
						"activation_url": activation_url
					})
				
				send_mail("Toonbook Activation Email", 
					get_template("activation_email.txt").render(context),
					getattr(settings, "EMAIL_HOST_USER", None),
					[request.POST["email"]],
					html_message = get_template("activation_email.html").render(context)
				)
				
				user = form.save(commit=False)
				user.activation = activation_url
				user.save()
				form.save_m2m()
				Album.objects.create(userid=toon.pk, title="My Pictures")

				return render_tb(request, "register_complete.html", {
					"title": "Register Complete"
				})
			
			else:
				return render_tb(request, "register.html", {
					"title": "Register",
					"form": form
				})
		else:
			return redirect("/")

class LogoutView(RESTView):
	@classonlymethod
	def get(self, request):
		request.session.flush()
		return redirect("/")

class ActivationView(RESTView):
	@classonlymethod
	def get(self, request):
		key = self.url_parameters["key"]
		
		try:
			#This should never happen, because Django won't allow the URL, but better safe than sorry
			if key == "" or key == None:
				raise ValueError()
			
			user = Toon.objects.get(activation=key)
		except:
			return render_tb(request, "activation_fail.html", {
				"title": "Activation Failure"
			})
		
		user.activation = ""
		user.save()
		
		return render_tb(request, "activation_success.html", {
			"title": "Activation Success",
			"username": user.username
		})

class PostView(RESTView):
	@classonlymethod
	def get(self, request):
		format = request.GET.get("format", "html")
		
		if is_logged_in(request):
			post_id = self.url_parameters["post"]
			user = get_user(request)
			
			try:
				post = Post.objects.get(pk=post_id)
			except Post.DoesNotExist:
				if format == "json":
					return JsonResponse({"success": False, "error_code": 1, "error": "Post does not exist."})
				else:
					raise Http404("Post does not exist.")
			
			if post.can_read(user):
				if format == "json":
					post_dict = model_to_dict(post)
					post_dict.pop("privacy", None)
					
					return JsonResponse(post_dict)
				else:
					poster = post.get_user()
					
					return render_tb(request, "standalone_post.html", {
						"title": "%s's post" % poster.toon_name,
						"post": get_template("figures/post.html").render(RequestContext(request, {
							"id": post.pk,
							"picture": str(poster.picture),
							"username": poster.username,
							"toon_name": poster.toon_name,
							"toon_id": poster.pk,
							"post": post.post,
							"broadcast": post.broadcast,
							"liked": Like.objects.filter(post_id=post.pk, post=True, userid=user.pk),
							"likes": Like.objects.filter(post_id=post.pk, post=True),
							"image_attachment": post.image_attachment,
							"date": post.date,
							"created": user.pk == post.userid,
							"comments": GetCommentView().get_comments(user, "html", post.pk),
							"standalone": True,
							"comment_form": CommentForm(initial={"post": post.pk})
						}))
					})
			else:
				if format == "json":
					return JsonResponse({"success": False, "error_code": 2, "error": "You are not allowed to read this post."})
				else:
					return HttpResponseForbidden("You are not allowed to read this post.") 
		else:
			if format == "json":
				return JsonResponse({"success": False, "error_code": 0, "error": "Not logged in."})
			else:
				return redirect("/login/")
	
	#TODO: CSRF protection
	@classonlymethod
	def patch(self, request):
		if is_logged_in(request):
			post_id = self.url_parameters["post"]
			
			try:
				post = Post.objects.get(pk=post_id)
			except Post.DoesNotExist:
				return JsonResponse({"success": False, "error_code": 1, "error": "Post does not exist."})
			
			user = get_user(request)
			poster = post.get_user()
			
			if post.can_read(user):
				try:
					like = Like.objects.get(userid=user.pk, post_id=post.pk, post=True)
				except Like.DoesNotExist:
					Like.objects.create(userid=user.pk, post_id=post.pk, post=True)

					try:
						notif = Notification.objects.get(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=1)
					except:
						Notification.objects.create(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=1)

						for comment in Comment.objects.filter(post=post.pk):
							Notification.objects.create(creator=user.pk, userid=comment.get_user().pk, link="/post/%d/" % post.pk, notif_type=3)
					else:
						notif.show = False
						notif.save()

					liked = True
				else:
					try:
						notif = Notification.objects.get(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=1, show=True)
					except:
						pass
					else:
						notif.show = False
						notif.save()

					for comment in Comment.objects.filter(post=post.pk):
						try:
							like_comment_notif = Notification.objects.get(creator=user.pk, userid=comment.get_user().pk, link="/post/%d/" % post.pk, notif_type=3)
						except Exception, e:
							print e
						else:
							like_comment_notif.delete()

					like.delete()
					liked = False
				
				return JsonResponse({"success": True, "liked": liked})
			else:
				return JsonResponse({"success": False, "error_code": 2, "error": "You are not allowed to read this post."})
		else:
			return JsonResponse({"success": False, "error_code": 0, "error": "Not logged in."})
	
	#TODO: CSRF protection
	@classonlymethod
	def delete(self, request):
		try:
			user = get_user(request)
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "Not logged in."})
		
		post_id = self.url_parameters["post"]
		
		try:
			post = Post.objects.get(pk=post_id)
		except Post.DoesNotExist:
			return JsonResponse({"success": False, "error_code": 1, "error": "Post does not exist."})
		
		if post.userid == user.pk:
			post.delete()
			return JsonResponse({"success": True})
		else:
			return JsonResponse({"success": False, "error_code": 2, "error": "Only the creator of that post can delete it."})

class CommentView(RESTView):
	@classonlymethod
	def get(self, request):
		format = request.GET.get("format", "html")
		
		if is_logged_in(request):
			cmnt_id = self.url_parameters["comment"]
			user = get_user(request)
			
			try:
				comment = Comment.objects.get(pk=cmnt_id)
			except Comment.DoesNotExist:
				return JsonResponse({"success": False, "error_code": 1, "error": "Comment does not exist."})
			
			post = comment.get_post()
			
			if post.can_read(user):
				comment_dict = model_to_dict(comment)
					
				return JsonResponse(comment_dict)
			else:
				return JsonResponse({"success": False, "error_code": 2, "error": "You are not allowed to read the post this comment is under."})
		else:
			if format == "json":
				return JsonResponse({"success": False, "error_code": 0, "error": "Not logged in."})
			else:
				return redirect("/login/")
	
	#TODO: CSRF protection
	@classonlymethod
	def patch(self, request):
		if is_logged_in(request):
			cmnt_id = self.url_parameters["comment"]
			
			try:
				comment = Comment.objects.get(pk=cmnt_id)
			except Comment.DoesNotExist:
				return JsonResponse({"success": False, "error_code": 1, "error": "Comment does not exist."})
			
			post = comment.get_post()
			user = get_user(request)
			poster = comment.get_user()
			
			if post.can_read(user):
				try:
					like = Like.objects.get(userid=user.pk, post_id=comment.pk, post=False)
				except Like.DoesNotExist:
					Like.objects.create(userid=user.pk, post_id=comment.pk, post=False)

					try:
						notif = Notification.objects.get(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=5)
					except Notification.DoesNotExist:
						Notification.objects.create(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=5)
					else:
						notif.show = False
						notif.save()

					liked = True
				else:
					try:
						notif = Notification.objects.get(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=5)
					except Notification.DoesNotExist:
						Notification.objects.create(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=5)
					else:
						notif.show = False
						notif.save()

					like.delete()
					liked = False
				
				return JsonResponse({"success": True, "liked": liked})
			else:
				return JsonResponse({"success": False, "error_code": 2, "error": "You are not allowed to read the post this is commented on."})
		else:
			return JsonResponse({"success": False, "error_code": 0, "error": "Not logged in."})
	
	#TODO: CSRF protection
	@classonlymethod
	def delete(self, request):
		try:
			user = get_user(request)
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "Not logged in."})
		
		comment_id = self.url_parameters["comment"]
		
		try:
			comment = Comment.objects.get(pk=comment_id)
		except Comment.DoesNotExist:
			return JsonResponse({"success": False, "error_code": 1, "error": "Comment does not exist."})
		
		post = comment.get_post()
		
		if comment.userid == user.pk or post.userid == user.pk:
			comment.delete()
			return JsonResponse({"success": True})
		else:
			return JsonResponse({"success": False, "error_code": 2, "error": "Only the creator of that comment or it's post can delete it."})

class ProfileView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			user = get_user(request)
		except:
			return redirect("/login/")
		
		try:
			profile = Toon.objects.get(username=self.url_parameters["profile"])
		except:
			raise Http404("Profile not found.")
		
		return render_tb(request, "profile.html", {
			"title": "%s's profile" % profile.toon_name,
			"profile": profile,
			"created": user.pk == profile.pk,
			"posts": GetPostView().get_posts(request, user, format, 10, profile=profile.pk)
		})

class ProfilePanelView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			user = get_user(request)
		except:
			return redirect("/login/")
		
		if user.username != self.url_parameters["profile"]:
			return redirect("/profile/%s/panel/" % user.username)
		
		return render_tb(request, "profile_panel.html", {
			"title": "Profile Panel",
			"form": ToonEditForm(initial={
				"toon_name": user.toon_name,
				"team": user.team,
				"species": user.species,
				"laff": user.laff,
				"toon_up": user.toon_up,
				"trap": user.trap,
				"lure": user.lure,
				"sound": user.sound,
				"throw": user.throw,
				"squirt": user.squirt,
				"drop": user.drop,
				"sellbot": user.sellbot,
				"cashbot": user.cashbot,
				"lawbot": user.lawbot,
				"bossbot": user.bossbot,
				"about_me": user.about_me,
				"notify_likes_post": user.notify_likes_post,
				"notify_likes_comment": user.notify_likes_comment,
				"notify_comments": user.notify_comments,
				"notify_liked_commented": user.notify_liked_commented,
				"notify_comment_commented": user.notify_comment_commented
			})
		})
	
	@classonlymethod
	def post(self, request):
		format = request.POST.get("format", "html")
		
		try:
			user = get_user(request)
		except:
			if format == "json":
				return JsonResponse({"success": False, "error_code": 0, "error": "You must be logged in."})
			else:
				return redirect("/login/")
		
		form = ToonEditForm(request.POST, request.FILES, instance=user)
		
		if form.is_valid():
			user.picture = form.cleaned_data["picture"] if form.cleaned_data["picture"] != "" else user.picture
			user.save()
			
			return redirect(request.META["HTTP_REFERER"])
		else:
			if format == "json":
				return JsonResponse({"success": False, "error_code": 1, "error": "Form was not valid."})
			else:
				return render_tb(request, "profile_panel.html", {
					"title": "Profile Panel",
					"form": form
				})

class ProfileImageView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			user = get_user(request)
		except:
			return redirect("/login/")
		
		profile = self.url_parameters["profile"]
		
		try:
			profile = Toon.objects.get(username=profile)
		except:
			return HttpResponseForbidden("Profile not found.") 
		
		return render_tb(request, "images.html", {
			"title": "Images",
			"albums": Album.objects.filter(userid=profile.pk),
			"profile": profile
		})

class ProfileImageAddView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			user = get_user(request)
		except:
			return redirect("/login/")

		if user.username != self.url_parameters["profile"]:
			return redirect("/profile/%s/images/add/" % user.username)

		return render_tb(request, "album_add.html", {
			"title": "Add Album",
			"form": AlbumForm(user=user)
		})

	@classonlymethod
	def post(self, request):
		try:
			user = get_user(request)
		except:
			return redirect("/login/")

		form = AlbumForm(request.POST, user=user)

		if form.is_valid():
			model = form.save(commit=False)
			model.userid = user.pk
			model.save()
			form.save_m2m()

			return redirect("/profile/%s/images/%d/" % (user.username, model.pk))
		else:
			return render_tb(request, "album_add.html", {
				"title": "Add Album",
				"form": form
			})

class ProfileAlbumView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			profile = Toon.objects.get(username=self.url_parameters["profile"])
		except:
			raise Http404("Profile not found.")

		try:
			album = Album.objects.get(userid=profile.pk, pk=int(self.url_parameters["album"]))
		except Exception, e:
			raise Http404("Album not found.")

		return render_tb(request, "album.html", {
			"title": album.title,
			"album": album
		})

	@classonlymethod
	#TODO: CSRF Protection
	def delete(self, request):
		try:
			profile = Toon.objects.get(username=self.url_parameters["profile"])
		except:
			raise Http404("Profile not found.")

		try:
			album = Album.objects.get(userid=profile.pk, pk=int(self.url_parameters["album"]))
		except Exception, e:
			raise Http404("Album not found.")

		try:
			user = get_user(request)
			assert user.pk == album.userid
			assert Album.objects.filter(userid=profile.pk)[0].pk != album.pk
		except Exception, e:
			return HttpResponseForbidden("You are not allowed to delete this album.")

		album.delete()
		return redirect(request.META["HTTP_REFERER"])

class NotificationsView(RESTView):
	@classonlymethod
	def get(self, request):
		format = request.GET.get("format", "json")

		try:
			user = get_user(request)
		except:
			if format == "json":
				return JsonResponse({"success": False, "error": "Not logged in.", "error_code": 1})
			else:
				return HttpResponseForbidden("Not logged in.")

		if format == "json":
			response = []

			for notif in Notification.objects.filter(userid=user.pk, show=True):
				dict_ = model_to_dict(notif)
				dict_.pop("show", None)
				response.append(dict_)

			return HttpResponse(json.dumps(response, cls=DateTimeEncoder), content_type='application/json')
		else:
			return render_tb(request, "figures/notifications.html", {})

class NotificationsReadAllView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			user = get_user(request)
		except:
			pass
		else:
			for notif in Notification.objects.filter(userid=user.pk):
				notif.delete()

		return HttpResponse(status=204)

class WritePostView(RESTView):
	@classonlymethod
	def post(self, request):
		if is_logged_in(request):
			form = PostForm(request.POST, request.FILES, user=get_user(request))
			
			if form.is_valid():
				user = get_user(request)
				timeout = getattr(settings, "DEFAULT_POST_TIMEOUT", 300)
				
				last_post = Post.objects.filter(userid=user.pk, broadcast=True).order_by("-pk")
				
				try:
					last_post = last_post[0]
				except IndexError:
					last_post = None
				
				if last_post != None and ((datetime.datetime.now(last_post.date.tzinfo) - last_post.date).total_seconds() < timeout) and request.POST["broadcast"]:
					return JsonResponse({"success": False, "error_code": 2, "error": "You have made a post too recently. You must wait %d seconds in between posts." % timeout}) #TODO: How many more seconds do you have to wait?
				
				post = form.save(commit=False)
				post.userid = get_user(request).pk
				post.save()
				form.save_m2m()
				
				return JsonResponse({"success": True, "post_id": post.pk})
			else:
				return JsonResponse({"success": False, "error_code": 1, "error": "Form is not valid. Have you filled out everything?"})
		else:
			return JsonResponse({"success": False, "error_code": 0, "error": "You must be logged in to make a post."})

class GetPostView(RESTView):
	def get_posts(self, request, user, format, since, to_response=False, profile=None):
		id = None
		
		i = 0
		
		check = Post.objects.all() if profile == None else Post.objects.filter(userid=profile)
		
		for post in check.order_by("-pk"):
			if post.can_read(user, profile):
				i += 1
				
				if i == int(since):
					id = post.pk
					break
		
		if i != int(since):
			id = 0
		
		posts = {
			"models": [],
			"dicts": []
		}
		
		for post in Post.objects.filter(pk__gte=id).order_by("-pk"):
			if profile != None and post.userid != profile:
				continue
			
			if post.can_read(user, profile):
				post_dict = model_to_dict(post)
				post_dict["liked"] = Like.objects.filter(post_id=post.pk, userid=user.pk, post=True).count() > 0
				post_dict.pop("privacy", None)
				posts["dicts"].append(post_dict)
				posts["models"].append(post)
		
		if format == "json":
			if to_response:
				return HttpResponse(json.dumps(posts["dicts"], cls=DateTimeEncoder), content_type='application/json')
			else:
				return json.dumps(posts["dicts"], cls=DateTimeEncoder)
		else:
			response = ""
			
			for post in posts["models"]:
				poster = post.get_user()
				
				response += get_template("figures/post.html").render(RequestContext(request, {
					"id": post.pk,
					"picture": poster.picture,
					"username": poster.username,
					"toon_name": poster.toon_name,
					"toon_id": poster.pk,
					"post": post.post,
					"broadcast": post.broadcast,
					"liked": Like.objects.filter(post_id=post.pk, userid=user.pk, post=True),
					"likes": Like.objects.filter(post_id=post.pk, post=True),
					"image_attachment": post.image_attachment,
					"created": user.pk == post.userid,
					"date": post.date,
					"comments": GetCommentView().get_comments(user, "html", post.pk),
					"standalone": False,
					"comment_form": CommentForm(initial={"post": post.pk})
				}))
			
			if to_response:
				return HttpResponse(response)
			else:
				return response
				
	@classonlymethod
	def get(self, request):
		format = request.GET.get("format", "json")
		since = request.GET.get("since", 10)
		profile = request.GET.get("profile", None)
		
		if profile != None:
			try:
				Toon.objects.get(pk=profile)
			except:
				profile = None
			else:
				profile = int(profile)
		
		try:
			user = get_user(request)
		except:
			user = None
		
		return GetPostView().get_posts(request, user, format, since, True, profile)

class WriteCommentView(RESTView):
	@classonlymethod
	def post(self, request):
		try:
			user = get_user(request)
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "You must be logged in to make a comment."})
		
		form = CommentForm(request.POST)
		
		if form.is_valid():
			post = Post.objects.get(pk=request.POST.get("post"))
			
			if post.can_read(user):
				poster = post.get_user()
				Notification.objects.create(creator=user.pk, userid=poster.pk, link="/post/%d/" % post.pk, notif_type=2)

				for comment in Comment.objects.filter(post=post.pk):
					Notification.objects.create(creator=user.pk, userid=comment.get_user().pk, link="/post/%d/" % post.pk, notif_type=4)

				comment = form.save(commit=False)
				comment.userid = user.pk
				comment.save()
				form.save_m2m()
				
				return JsonResponse({"success": True})
			else:
				return JsonResponse({"success": False, "error_code": 2, "error": "You are not allowed to read that post."})
		else:
			return JsonResponse({"success": False, "error_code": 1, "error": "Error validating form. Have you filled in everything?"})

class GetCommentView(RESTView):
	def get_comments(self, user, format, post_id, to_response=False):
		try:
			post = Post.objects.get(pk=post_id)
		except Post.DoesNotExist:
			if to_response:
				if format == "html":
					raise Http404("Post not found.")
				else:
					return JsonResponse({"success": False, "error_code": 0, "error": "Post not found."})
			else:
				if format == "html":
					raise
				else:
					return {"success": False, "error_code": 0, "error": "Post not found."}
		
		if post.can_read(user):
			if format == "html":
				response = ""
				
				for comment in Comment.objects.filter(post=post_id):
					poster = comment.get_user()
				
					response += get_template("figures/comment.html").render(Context( {
						"id": comment.pk,
						"picture": poster.picture,
						"username": poster.username,
						"toon_name": poster.toon_name,
						"toon_id": poster.pk,
						"post": comment.comment,
						"liked": Like.objects.filter(post_id=comment.pk, post=False, userid=user.pk),
						"likes": Like.objects.filter(post_id=comment.pk, post=False),
						"created": ((user.pk == poster.pk or user.pk == post.userid) if user != None else False),
						"date": comment.date
					}))
				
				if to_response:
					return HttpResponse(response)
				else:
					return response
			else:
				json_r = []
				
				for comment in Comment.objects.filter(post=post_id):
					poster = comment.get_user()
					
					json_r.append({
						"id": comment.pk,
						"userid": poster.pk,
						"toon_name": poster.toon_name,
						"toon_id": poster.pk,
						"post": comment.comment,
						"liked": Like.objects.filter(post_id=comment.pk, post=False, userid=user.pk),
						"likes": Like.objects.filter(post_id=comment.pk, post=False),
						"image_attachment": post.image_attachment.pk,
						"created": ((user.pk == poster.pk or user.pk == post.userid) if user != None else False),
						"date": comment.date
					})
				
				if to_response:
					return HttpResponse(json.dumps(json_r, cls=DateTimeEncoder), content_type='application/json')
				else:
					return json.dumps(json_r, cls=DateTimeEncoder)
		else:
			if format == "html":
				if to_response:
					return HttpResponseForbidden("You are not allowed to read this post.")
				else:
					return ""
			else:
				if to_response:
					return JsonResponse({"success": False, "error_code": 1, "error": "You are not allowed to read this post."})
				else:
					return {"success": False, "error_code": 1, "error": "You are not allowed to read this post."}
	
	@classonlymethod
	def get(self, request):
		format = request.GET.get("format", "json")
		post = request.GET.get("post", None)
		
		try:
			user = get_user(request)
		except:
			user = None
		
		return GetCommentView().get_comments(user, format, post, True)

class AdminView(RESTView):
	@classonlymethod
	def get(self, request):
		try:
			user = get_user(request)
			
			if not user.admin:
				raise Exception()
		except:
			return HttpResponseForbidden("You are not allowed to use the admin panel.")
		
		return render_tb(request, "admin.html", {
			"title": "Admin Panel",
			"filter_form": FilterForm(),
			"filters": Filter.objects.all(),
			"accounts": Toon.objects.filter(activation="", approved=False)
		})

class AdminFilterView(RESTView):
	@classonlymethod
	def post(self, request):
		try:
			user = get_user(request)
			
			if not user.admin:
				raise Exception()
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "You are not allowed to use the admin panel."})
		
		form = FilterForm(request.POST)
		
		if form.is_valid():
			form.save()
			
			return redirect("/admin_panel/")
		else:
			return JsonResponse({"success": False, "error_code": 1, "error": "Something went wrong."})
	
	@classonlymethod
	#TODO: CSRF Protection
	def delete(self, request):
		try:
			user = get_user(request)
			
			if not user.admin:
				raise Exception()
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "You are not allowed to use the admin panel."})
		
		try:
			filter = Filter.objects.get(pk=QueryDict(request.body).get("id"))
		except:
			return JsonResponse({"success": False, "error_code": 1, "error": "Filter not found."})
		
		filter.delete()
		return redirect("/admin_panel/")

class AdminApproveView(RESTView):
	@classonlymethod
	#TODO: CSRF Protection
	def patch(self, request):
		try:
			user = get_user(request)
			
			if not user.admin:
				raise Exception()
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "You are not allowed to use the admin panel."})
		
		try:
			user = Toon.objects.get(pk=QueryDict(request.body).get("id"))
		except:
			return JsonResponse({"success": False, "error_code": 1, "error": "User not found."})
		
		user.approved = True
		user.save()
		
		return redirect("/admin_panel/")
	
	#TODO: CSRF Protection
	def delete(self, request):
		try:
			user = get_user(request)
			
			if not user.admin:
				raise Exception()
		except:
			return JsonResponse({"success": False, "error_code": 0, "error": "You are not allowed to use the admin panel."})
		
		try:
			user = Toon.objects.get(pk=QueryDict(request.body).get("id"))
		except:
			return JsonResponse({"success": False, "error_code": 1, "error": "User not found."})
		
		user.delete()
		
		return redirect("/admin_panel/")
