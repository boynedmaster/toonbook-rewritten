from django.contrib import admin
from .models import *

admin.site.register(Toon)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Like)
admin.site.register(Album)
admin.site.register(Image)
admin.site.register(Notification)
