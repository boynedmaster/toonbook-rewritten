from django.core.validators import MaxValueValidator, MinValueValidator, MaxLengthValidator
from django.db import models
from django.utils import timezone
from enum import Enum

#All the enums
class Team(Enum):
	NONE = 0
	TOMATO = 1
	POTATO = 2
	VENI = 3
	MEME = 4
	KID_SQUIDS = 5
	IVY = 6
	LOUIE = 7
	YUCIE = 8
	TAFFY = 9
	SLAPPY = 10
	FLIPPY = 11
	JOESTOON = 12
	CRINKLEDOODLE = 13
	JOEY = 14
	MCSTINK = 15
	FOX = 16
	YIPPIE = 17
	LILLY = 18
	VILOT = 19
	MASTER_BART = 20
	
	labels = {
		NONE: "",
		TOMATO: "Tomato",
		POTATO: "Potato",
		VENI: "Veni",
		MEME: "Meme",
		KID_SQUIDS: "Kid Squids",
		IVY: "Ivy",
		LOUIE: "Louie",
		YUCIE: "Yucie",
		TAFFY: "Taffy",
		SLAPPY: "Slappy",
		FLIPPY: "Flippy",
		JOESTOON: "Joestoon",
		CRINKLEDOODLE: "Crinkledoodle",
		JOEY: "Joey",
		MCSTINK: "McStink",
		FOX: "Fox",
		YIPPIE: "Yippie",
		LILLY: "Lilly",
		VILOT: "Vilot",
		MASTER_BART: "Master Bart"
	}

class Species(Enum):
	CAT = 0
	DOG = 1
	MOUSE = 2
	HORSE = 3
	DUCK = 4
	RABBIT = 5
	MONKEY = 6
	BEAR = 7
	PIG = 8
	
	labels = {
		CAT: "Cat",
		DOG: "Dog",
		MOUSE: "Mouse",
		HORSE: "Horse",
		DUCK: "Duck",
		RABBIT: "Rabbit",
		MONKEY: "Monkey",
		BEAR: "Bear",
		PIG: "Pig"
	}

class Toonup(Enum):
	NONE = 0
	FEATHER = 1
	MEGAPHONE = 2
	LIPSTICK = 3
	BAMBOO_CANE = 4
	PIXIE_DUST = 5
	JUGGLING_CUBES = 6
	HIGH_DIVE = 7
	
	labels = {
		NONE: "",
		FEATHER: "Feather",
		MEGAPHONE: "Megaphone",
		LIPSTICK: "Lipstick",
		BAMBOO_CANE: "Bamboo Cane",
		PIXIE_DUST: "Pixie Dust",
		JUGGLING_CUBES: "Juggling Cubes",
		HIGH_DIVE: "High Dive"
	}

class Trap(Enum):
	NONE = 0
	BANANA_PEEL = 1
	MARBLES = 2
	RAKE = 3
	QUICKSAND = 4
	TRAPDOOR = 5
	TNT = 6
	RAILROAD = 7
	
	labels = {
		NONE: "",
		BANANA_PEEL: "Banana Peel",
		MARBLES: "Marbles",
		RAKE: "Rake",
		QUICKSAND: "Quicksand",
		TRAPDOOR: "Trapdoor",
		TNT: "TNT",
		RAILROAD: "Railroad"
	}

class Lure(Enum):
	NONE = 0
	ONE_DOLLAR = 1
	SMALL_MAGNET = 2
	FIVE_DOLLAR = 3
	BIG_MAGNET = 4
	TEN_DOLLAR = 5
	HYPNO_GOGGLES = 6
	PRESENTATION = 7
	
	labels = {
		NONE: "",
		ONE_DOLLAR: "$1 Bill",
		SMALL_MAGNET: "Small Magnet",
		FIVE_DOLLAR: "$5 Bill",
		BIG_MAGNET: "Big Magnet",
		TEN_DOLLAR: "$10 Bill",
		HYPNO_GOGGLES: "Hypno Goggles",
		PRESENTATION: "Presentation"
	}

class Sound(Enum):
	NONE = 0
	BIKE_HORN = 1
	WHISTLE = 2
	BUGLE = 3
	AOOGAH = 4
	TRUNK = 5
	FOG = 6
	OPERA = 7
	
	labels = {
		NONE: "",
		BIKE_HORN: "Bike Horn",
		WHISTLE: "Whistle",
		BUGLE: "Bugle",
		AOOGAH: "Aoogah",
		TRUNK: "Trunk",
		FOG: "Foghorn",
		OPERA: "Opera"
	}

class Throw(Enum):
	CUPCAKE = 0
	FRUIT_SLICE = 1
	CREAM_SLICE = 2
	FRUIT_PIE = 3
	CREAM_PIE = 4
	BDAY_CAKE = 5
	WEDDING_CAKE = 6
	
	labels = {
		CUPCAKE: "Cupcake",
		FRUIT_SLICE: "Fruit Pie Slice",
		CREAM_SLICE: "Cream Pie Slice",
		FRUIT_PIE: "Fruit Pie",
		CREAM_PIE: "Cream Pie",
		BDAY_CAKE: "Birthday Cake",
		WEDDING_CAKE: "Wedding Cake"
	}

class Squirt(Enum):
	FLOWER = 0
	GLASS = 1
	GUN = 2
	SELTZER = 3
	HOSE = 4
	STORM = 5
	GEYSER = 6
	
	labels = {
		FLOWER: "Squirting Flower",
		GLASS: "Glass of Water",
		GUN: "Squirt Gun",
		SELTZER: "Seltzer Bottle",
		HOSE: "Fire Hose",
		STORM: "Storm Cloud",
		GEYSER: "Geyser"
	}

class Drop(Enum):
	NONE = 0
	POT = 1
	SANDBAG = 2
	ANVIL = 3
	BIG_WEIGHT = 4
	SAFE = 5
	PIANO = 6
	TOONTANIC = 7
	
	labels = {
		NONE: "",
		POT: "Flower Pot",
		SANDBAG: "Sandbag",
		ANVIL: "Anvil",
		BIG_WEIGHT: "Big Weight",
		SAFE: "Safe",
		PIANO: "Grand Piano",
		TOONTANIC: "Toontanic"
	}

#i got tired of writing out every single name, have some unreadable acronyms
class SellbotSuit(Enum):
	NONE = 0
	CC = 1
	T = 2
	ND = 3
	GH = 4
	MS = 5
	TF = 6
	TM = 7
	MH = 8
	MH_M = 9
	
	labels = {
		NONE: "",
		CC: "Cold Caller",
		T: "Telemarketer",
		ND: "Name Dropper",
		GH: "Glad Hander",
		MS: "Mover & Shaker",
		TF: "Two-Face",
		TM: "The Mingler",
		MH: "Mr. Hollywood",
		MH_M: "Maxed Mr. Hollywood"
	}

class CashbotSuit(Enum):
	NONE = 0
	SC = 1
	PP = 2
	TW = 3
	BC = 4
	NC = 5
	MB = 6
	LS = 7
	RB = 8
	RB_M = 9
	
	labels = {
		NONE: "",
		SC: "Short Change",
		PP: "Penny Pincher",
		TW: "Tightwad",
		BC: "Bean Counter",
		NC: "Number Cruncher",
		MB: "Money Bags",
		LS: "Loan Shark",
		RB: "Robber Baron",
		RB_M: "Maxed Robber Baron"
	}

class LawbotSuit(Enum):
	NONE = 0
	BF = 1
	BS = 2
	DT = 3
	AC = 4
	B = 5
	SD = 6
	LE = 7
	BW = 8
	BW_M = 9
	
	labels = {
		NONE: "",
		BF: "Bottom Feeder",
		BS: "Bloodsucker",
		DT: "Double Talker",
		AC: "Ambulance Chaser",
		B: "Backstabber",
		SD: "Spin Doctor",
		LE: "Legal Eagle",
		BW: "Big Wig",
		BW_M: "Maxed Big Wig"
	}

class BossbotSuit(Enum):
	NONE = 0
	F = 1
	PP = 2
	YM = 3
	MM = 4
	DS = 5
	HH = 6
	CR = 7
	BC = 8
	BC_M = 9
	
	labels = {
		NONE: "",
		F: "Flunky",
		PP: "Pencil Pusher",
		YM: "Yesman",
		MM: "Micromanager",
		DS: "Downsizer",
		HH: "Head Hunter",
		CR: "Corporate Raider",
		BC: "The Big Cheese",
		BC_M: "Maxed The Big Cheese"
	}

#Basic Toon model
class Toon(models.Model):
	email = models.EmailField()
	username = models.CharField(max_length=32)
	password = models.CharField(max_length=100)
	birthday = models.DateField()
	activation = models.CharField(max_length=100, blank=True)
	approved = models.BooleanField(default=False)
	picture = models.BinaryField(default="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAvCAMAAACSXLn7AAAB+FBMVEWwvNWyvta2wdi3wtm3w9q4wtq4w9m4w9q5xNq5xNu6xNu6xdu7xdu7xdy7xtu7xty8xtu8xty8x9y9xty9x9y9yNy+x92+yNy+yN2/yd2/yd7Ayd7Ayt7Byt7By97By9/Cy9/CzN/DzN/DzODDzeDEzODEzeDEzuDFzeDFzuDFzuHGzuHGz+HH0OLI0OLI0eLJ0eLJ0ePJ0uPK0uPK0uTK0+PL0+PL0+TL1OTM0+PM0+TM1OXN1eTN1eXO1eXO1uXP1uXP1+bQ1+bQ2OfR2ObR2OfS2OfS2efT2ejT2ujU2ujU2unU2+jV2+jV2+nV3OnW3OnW3OrX3erX3urY3urY3uvZ3+vZ4Oza3+va4Oza4ezb4ezc4ezc4uzc4u3d4u7e4+7e5O/f5O7f5O/f5e7f5e/g5O7g5O/g5e/h5e/h5u/i5/Dj5/Dj5/Hj6PHk5/Dk5/Hl6fHl6vHm6vLn6vLn6/Ln6/Po6/Po7PPo7PTp7PPp7fTr7vXs7vXs7/Xs7/bs8Pbt7/Xt8Pbt8ffu8Pbu8fbv8fbv8vfw8vfw8/fw8/jx8/jy9Pjz9fjz9fn09fn09fr09vn19vn29/r29/v2+Pr3+Pv3+fv4+fv5+vv5+v37+/37/P38/P38/f38/f79/P79/f79/v7+/v7+/v/+/////v/////m30VYAAACRklEQVR4Ae3U61sMYRjH8RyizJj15MHstmtnZ9bTzrSIUkSKJESHEHKQc5FDDiLJQRGRQ6XQxrL8/k3X5pLdmbkXb7zyff+5nvt+8dxZ4i/7d+A/CDCFLZJUkcwwMgFNM4QZzFlRd6Kr41yVZ6kZkBWmWRSwtHy/wpS8Yx8xXffKueGmnpvRAAV8xcNvH+wpHsDP3p8ZA7BDpoBcBwAJpPS66gbaaVCP09JLpPZBykGfRIIuRMsB4Ov0S4lvAB5nvxpVdQrcRUE/8GVmqiQ0OiH8FLiDshHbEqjajgpOgYdviuOwdTSKFtkd6HmPxnfBXksYbQQIs/vYMAxb6zZjNyNG8lyAdQrpTanNqKGA0oxNZbBVUoJaCshNqD0IW9cqsJMC6nr0PIetkXb6hYAZm5iArdsHsIUTIBicvHcJtqKNKPISQCjPXqxOIK22rPZPWpACchfkI0jtKZt9edQbooDUCiP7JH7Vq6ns7CA3KMBrUclyS/vxo3eNkmrx/VdUQYGQuFjpF4xXNPQC2JcnG2bmM2PqS4yIiOjLZm0EUJQrkumaHTgrULRJxMNK5E8OmanzhQu8DTGglcmc6+ZvQAFj1TWNI0g2Vl9evZhlBJZvzpo+pNZfOs9rkcBSQx1x2Lq6nFsU8PuG4GxylWy5g8j843BrwBN0BSYvnIJrh3MibsCQb8G9mOCmE5jSVlB1ewwn8JvjINsrO0EgPASqxDbuBGa+MkjtUKhYTiC8oQkCfF7LhQtgnQB6DiGt+PnrAJ74Zn7pdw78l0j4dpH7AAAAAElFTkSuQmCC")
	admin = models.BooleanField(default=False)
	vip = models.BooleanField(default=False)
	banned = models.BooleanField(default=False)
	
	#Toon info
	toon_name = models.CharField(max_length=32)
	team = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Team) - 2)], default=0)
	species = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Species) - 2)], default=Species.CAT.value)
	laff = models.IntegerField(validators=[MinValueValidator(15), MaxValueValidator(137)], default=15)
	toon_up = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Toonup) - 2)], default=Toonup.NONE.value)
	trap = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Trap) - 2)], default=Trap.NONE.value)
	lure = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Lure) - 2)], default=Lure.NONE.value)
	sound = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Sound) - 2)], default=Sound.NONE.value)
	throw = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Throw) - 2)], default=Throw.CUPCAKE.value)
	squirt = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Squirt) - 2)], default=Squirt.FLOWER.value)
	drop = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(Drop) - 2)], default=Drop.NONE.value)
	sellbot = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(SellbotSuit) - 2)], default=SellbotSuit.NONE.value)
	cashbot = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(CashbotSuit) - 2)], default=CashbotSuit.NONE.value)
	lawbot = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(LawbotSuit) - 2)], default=LawbotSuit.NONE.value)
	bossbot = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(len(BossbotSuit) - 2)], default=BossbotSuit.NONE.value)
	about_me = models.CharField(max_length=1000, null=True, blank=True)

	#Settings
	#Notification values
	notify_likes_post = models.BooleanField(default=True) #Someone liked your post
	notify_likes_comment = models.BooleanField(default=True) #Someoned liked your comment
	notify_comments = models.BooleanField(default=True) #Someone commented on your post
	notify_liked_commented = models.BooleanField(default=False) #Someoned liked a post you commented on
	notify_comment_commented = models.BooleanField(default=False) #Someoned commented on a post you commented on

class Post(models.Model):
	post = models.CharField(max_length=300)
	broadcast = models.BooleanField(default=False)
	privacy = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(2)], default=2)
	userid = models.IntegerField()
	date = models.DateTimeField(default=timezone.now)
	image_attachment = models.ForeignKey("Image", null=True, on_delete=models.CASCADE)
	
	def can_read(self, user, profile=None):
		if user != None:
			if self.userid == user.pk:
				return True
		
		#TODO: Friends only + privacy 2 w/ friends
		
		if self.privacy == 2:
			if self.broadcast or profile != None:
				return True
		
		return False
	
	def get_user(self):
		return Toon.objects.get(pk=self.userid)

class Comment(models.Model):
	comment = models.CharField(max_length=300)
	userid = models.IntegerField()
	post = models.IntegerField()
	date = models.DateTimeField(default=timezone.now)
	
	def get_user(self):
		return Toon.objects.get(pk=self.userid)
	
	def get_post(self):
		return Post.objects.get(pk=self.post)

class Like(models.Model):
	userid = models.IntegerField()
	post_id = models.IntegerField()
	post = models.BooleanField(default=True)
	
	def get_post(self):
		if self.post:
			return Post.objects.get(pk=self.post_id)
		else:
			return Comment.objects.get(pk=self.post_id)
	
	def get_user(self):
		return Toon.objects.get(pk=self.userid)

class Filter(models.Model):
	replace = models.CharField(max_length=300)
	with_this = models.CharField(max_length=300)

class Album(models.Model):
	userid = models.IntegerField()
	title = models.CharField(max_length=100)
	
	def get_user(self):
		return Toon.objects.get(pk=self.userid)

	def get_images(self):
		return Image.objects.filter(album=self.pk)

class Image(models.Model):
	image = models.BinaryField()
	album = models.IntegerField()
	userid = models.IntegerField()

	def get_user(self):
		return Toon.objects.get(pk=self.userid)
	
	def get_album(self):
		return Album.objects.get(pk=self.album)

class Notification(models.Model):
	creator = models.IntegerField()
	userid = models.IntegerField()
	link = models.CharField(max_length=255)
	notif_type = models.IntegerField() #1: Liked post, 2: Comment on post, 3: Liked post you commented on, 4: Commented on post you commented on, 5: Liked comment 
	show = models.BooleanField(default=True) #Use this to prevent like spamming

	def get_creator(self):
		return Toon.objects.get(pk=self.creator)

	def get_user(self):
		return Toon.objects.get(pk=self.userid)

	def save(self, *args, **kwargs):
		if self.creator == self.userid:
			return
		else:
			user = self.get_user()
			valid = [
				user.notify_likes_post,
				user.notify_comments,
				user.notify_liked_commented,
				user.notify_comment_commented,
				user.notify_likes_comment
			]

			if valid[self.notif_type - 1]:
				super(Notification, self).save(*args, **kwargs)
