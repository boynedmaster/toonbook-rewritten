$(document).ready(function(){
	$(".write_post").submit(function(){
		var formdata = new FormData($(this)[0]);

		$.ajax({
			url: "/write_post/", 
			data: formdata ? formdata : $(this).serialize(),
			cache: false,
			contentType: false,
			processData: false,
			type: "POST",
			success: function(data){
				if(data["error"] != undefined){
					$("#post-error").show();
					$("#post-error-text").text("Error code "+data["error_code"] + " - " + data["error"]);
				}else{
					$(".write_post")[0].reset();
					alert("Posted!");
					refreshPosts();
				}
			}
		});
		
		return false;
	});
	
	$(window).scroll(function(){
		if($(document).height() <= ($(window).height() + $(window).scrollTop())){
			base += 10;
			refreshPosts();
		}
	});
	
	setInterval(function(){
		refreshPosts();
	}, 60 * 1000);
	
	$("#refresh").click(function(){
		refreshPosts();
	});
});
