refreshPosts = function(){
	$.get("/get_posts/", {since: base, format: "html", profile: profile_id}, function(data){
		$(".posts").html(data);
	});
}

$(document).ready(function(){
	$(window).scroll(function(){
		if($(document).height() <= ($(window).height() + $(window).scrollTop())){
			base += 10;
			refreshPosts();
		}
	});
	
	setInterval(function(){
		refreshPosts();
	}, 60 * 1000);
	
	$("#profile-info").click(function(){
		$("#profile-info tr:nth-child(n+5)").toggle();
	});
});
