var base = 10;
	
function csrfSafeMethod(method) {
	// these HTTP methods do not require CSRF protection
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

var csrftoken = Cookies.get('csrftoken');

if(!window.location.origin){
	window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
}

function refreshPosts(){
	$.get("/get_posts/", {since: base, format: "html"}, function(data){
		$(".posts").html(data);
	});
}

function refreshNotifications(){
	$.get("/notifications/", {format: "html"}, function(data){
		$("#notifications-window").html(data);
	});
}

$(document).ready(function(){
	$.expr[':'].childof = function(obj, index, meta, stack){
		return $(obj).closest(meta[3]).is(meta[3]);
	};

	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			if(!csrfSafeMethod(settings.type) && !this.crossDomain){
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});

	setInterval(refreshNotifications, 30 * 1000);

	$("body").click(function(e){
		var target = $(e.target);

		if(target.is(":not(:childof(.notifications-bar))") && $(".notifications-bar").css("display") != "none"){
			$(".notifications-bar").hide();
		}

		if(target.is("#read_all")){
			$.get("/notifications/read_all/", refreshNotifications);
		}

		if(target.is(".delete-action-post")){
			if(confirm("Are you sure you want to delete this post?")){
				var post = target.parents(".post");
				var id = post.attr("data-id");
				
				$.ajax({
					type: "DELETE",
					url: "/post/"+id+"/",
					success: function(){
						alert("Post deleted.");
						refreshPosts();
					}
				});
			}
		}
		
		if(target.is(".like-action-post")){
			var post = target.parents(".post");
			var id = post.attr("data-id");
			
			$.ajax({
				type: "PATCH",
				url: "/post/"+id+"/",
				success: refreshPosts
			});
		}
		
		if(target.is(".delete-action-comment")){
			if(confirm("Are you sure you want to delete this comment?")){
				var comment = target.parents(".comment");
				var id = comment.attr("data-id");
				
				$.ajax({
					type: "DELETE",
					url: "/comment/"+id+"/",
					success: function(){
						alert("Comment deleted.");
						refreshPosts();
					}
				});
			}
		}
		
		if(target.is(".like-action-comment")){
			var comment = target.parents(".comment");
			var id = comment.attr("data-id");
			
			$.ajax({
				type: "PATCH",
				url: "/comment/"+id+"/",
				success: refreshPosts
			});
		}
		
		if(target.is(".share-action")){
			var post = target.parents(".post");
			var id = post.attr("data-id");
			
			prompt("Permalink for this post:", window.location.origin + "/post/"+id+"/");
		}
		
		if(target.is(".comment-action")){
			var post = target.parents(".post");
			post.find("form").show();
		}
		
		if(target.is("tr.image-attachment img")){
			$("div.image-attachment img").attr("src", target.attr("src"));
			$("div.image-attachment").show();
		}

		if(target.is("div.image-attachment:not(img)")){
			$("div.image-attachment").hide();
		}

		if(target.is("#notifications span")){
			$(".notifications-bar").show();
			refreshNotifications();
		}

		if(target.is("a[href='#']"))
			return false;
	});
	
	$("body").submit(function(e){
		var target = $(e.target);
		
		if(target.is(".submit_comment")){
			$.post("/write_comment/", target.serialize(), function(data){
				if(data["error"] != undefined){
					var error = target.find(".comment-error");
					
					error.show();
					error.find(".comment-error-text").text("Error code "+data["error_code"] + " - " + data["error"]);
				}else{
					alert("Commented!");
					refreshPosts();
				}
			});
			
			return false;
		}
		
		if(target.is("form[method='DELETE']")){
			$.ajax({
				type: "DELETE",
				url: target.attr("action"),
				data: target.serialize(),
				success: function(response){
					window.location.reload();
				},
				error: function(response){
					window.location.reload();
				}
			});
			
			return false;
		}
		
		if(target.is("form[method='PATCH']")){
			$.ajax({
				type: "PATCH",
				url: target.attr("action"),
				data: target.serialize(),
				success: function(response){
					window.location.reload();
				},
				error: function(response){
					window.location.reload();
				}
			});
			
			return false;
		}
	});
	
	$("#profile").hover(function(){
		$(".profile-actions").show();
	});
	
	$(".profile-actions").on("mouseleave", function(){
		$(this).hide();
	});
});
